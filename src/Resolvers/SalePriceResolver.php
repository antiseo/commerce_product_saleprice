<?php

namespace Drupal\commerce_product_saleprice\Resolvers;

use Drupal\commerce\Context;
use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_price\Resolver\PriceResolverInterface;
use Drupal\commerce_price\Price;
use \Drupal\user\Entity\User;

/**
 * Class SalePriceResolver.
 *
 * @package Drupal\commerce_product_saleprice\Resolvers
 */
class SalePriceResolver implements PriceResolverInterface {

    /**
     * {@inheritdoc}
     */
    public function resolve(PurchasableEntityInterface $entity, $quantity, Context $context) {
        $current_user_id = \Drupal::currentUser()->id();
        $account = User::load($current_user_id);

        if ($account->hasRole('optovik')) {
            $role_price = $entity->get('field_opt_price');
            return new Price($role_price->number, $role_price->currency_code);
        }
        return $entity->getPrice();
    }

}